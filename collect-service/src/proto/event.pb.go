// Code generated by protoc-gen-gogo. DO NOT EDIT.
// source: proto/event.proto

/*
	Package vlek is a generated protocol buffer package.

	It is generated from these files:
		proto/event.proto
		proto/event-store.proto

	It has these top-level messages:
		Event
		AggregateRequest
		AggregateResponse
*/
package vlek

import proto "github.com/gogo/protobuf/proto"
import fmt "fmt"
import math "math"
import google_protobuf "github.com/gogo/protobuf/types"
import _ "github.com/gogo/protobuf/types"
import _ "github.com/gogo/protobuf/gogoproto"

import time "time"

import github_com_gogo_protobuf_types "github.com/gogo/protobuf/types"

import io "io"

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf
var _ = time.Kitchen

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.GoGoProtoPackageIsVersion2 // please upgrade the proto package

// Generic Event Message
type Event struct {
	// ID to identify the Aggregate
	Id string `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	// Increasing Version of the Aggregate
	Version int32 `protobuf:"varint,2,opt,name=version,proto3" json:"version,omitempty"`
	// ID to identify the initiator of the Event
	Actor string `protobuf:"bytes,3,opt,name=actor,proto3" json:"actor,omitempty"`
	// Optional payload, required to execute the Event
	Data *google_protobuf.Any `protobuf:"bytes,4,opt,name=data" json:"data,omitempty"`
	// Event Name
	EventName string `protobuf:"bytes,5,opt,name=eventName,proto3" json:"eventName,omitempty"`
	// Process ID, inherited by command
	ProcessId string `protobuf:"bytes,6,opt,name=processId,proto3" json:"processId,omitempty"`
	// Timestamp when the Event was issued
	Timestamp *time.Time `protobuf:"bytes,7,opt,name=timestamp,stdtime" json:"timestamp,omitempty"`
}

func (m *Event) Reset()                    { *m = Event{} }
func (m *Event) String() string            { return proto.CompactTextString(m) }
func (*Event) ProtoMessage()               {}
func (*Event) Descriptor() ([]byte, []int) { return fileDescriptorEvent, []int{0} }

func (m *Event) GetId() string {
	if m != nil {
		return m.Id
	}
	return ""
}

func (m *Event) GetVersion() int32 {
	if m != nil {
		return m.Version
	}
	return 0
}

func (m *Event) GetActor() string {
	if m != nil {
		return m.Actor
	}
	return ""
}

func (m *Event) GetData() *google_protobuf.Any {
	if m != nil {
		return m.Data
	}
	return nil
}

func (m *Event) GetEventName() string {
	if m != nil {
		return m.EventName
	}
	return ""
}

func (m *Event) GetProcessId() string {
	if m != nil {
		return m.ProcessId
	}
	return ""
}

func (m *Event) GetTimestamp() *time.Time {
	if m != nil {
		return m.Timestamp
	}
	return nil
}

func init() {
	proto.RegisterType((*Event)(nil), "vlek.Event")
}
func (m *Event) Marshal() (dAtA []byte, err error) {
	size := m.Size()
	dAtA = make([]byte, size)
	n, err := m.MarshalTo(dAtA)
	if err != nil {
		return nil, err
	}
	return dAtA[:n], nil
}

func (m *Event) MarshalTo(dAtA []byte) (int, error) {
	var i int
	_ = i
	var l int
	_ = l
	if len(m.Id) > 0 {
		dAtA[i] = 0xa
		i++
		i = encodeVarintEvent(dAtA, i, uint64(len(m.Id)))
		i += copy(dAtA[i:], m.Id)
	}
	if m.Version != 0 {
		dAtA[i] = 0x10
		i++
		i = encodeVarintEvent(dAtA, i, uint64(m.Version))
	}
	if len(m.Actor) > 0 {
		dAtA[i] = 0x1a
		i++
		i = encodeVarintEvent(dAtA, i, uint64(len(m.Actor)))
		i += copy(dAtA[i:], m.Actor)
	}
	if m.Data != nil {
		dAtA[i] = 0x22
		i++
		i = encodeVarintEvent(dAtA, i, uint64(m.Data.Size()))
		n1, err := m.Data.MarshalTo(dAtA[i:])
		if err != nil {
			return 0, err
		}
		i += n1
	}
	if len(m.EventName) > 0 {
		dAtA[i] = 0x2a
		i++
		i = encodeVarintEvent(dAtA, i, uint64(len(m.EventName)))
		i += copy(dAtA[i:], m.EventName)
	}
	if len(m.ProcessId) > 0 {
		dAtA[i] = 0x32
		i++
		i = encodeVarintEvent(dAtA, i, uint64(len(m.ProcessId)))
		i += copy(dAtA[i:], m.ProcessId)
	}
	if m.Timestamp != nil {
		dAtA[i] = 0x3a
		i++
		i = encodeVarintEvent(dAtA, i, uint64(github_com_gogo_protobuf_types.SizeOfStdTime(*m.Timestamp)))
		n2, err := github_com_gogo_protobuf_types.StdTimeMarshalTo(*m.Timestamp, dAtA[i:])
		if err != nil {
			return 0, err
		}
		i += n2
	}
	return i, nil
}

func encodeFixed64Event(dAtA []byte, offset int, v uint64) int {
	dAtA[offset] = uint8(v)
	dAtA[offset+1] = uint8(v >> 8)
	dAtA[offset+2] = uint8(v >> 16)
	dAtA[offset+3] = uint8(v >> 24)
	dAtA[offset+4] = uint8(v >> 32)
	dAtA[offset+5] = uint8(v >> 40)
	dAtA[offset+6] = uint8(v >> 48)
	dAtA[offset+7] = uint8(v >> 56)
	return offset + 8
}
func encodeFixed32Event(dAtA []byte, offset int, v uint32) int {
	dAtA[offset] = uint8(v)
	dAtA[offset+1] = uint8(v >> 8)
	dAtA[offset+2] = uint8(v >> 16)
	dAtA[offset+3] = uint8(v >> 24)
	return offset + 4
}
func encodeVarintEvent(dAtA []byte, offset int, v uint64) int {
	for v >= 1<<7 {
		dAtA[offset] = uint8(v&0x7f | 0x80)
		v >>= 7
		offset++
	}
	dAtA[offset] = uint8(v)
	return offset + 1
}
func (m *Event) Size() (n int) {
	var l int
	_ = l
	l = len(m.Id)
	if l > 0 {
		n += 1 + l + sovEvent(uint64(l))
	}
	if m.Version != 0 {
		n += 1 + sovEvent(uint64(m.Version))
	}
	l = len(m.Actor)
	if l > 0 {
		n += 1 + l + sovEvent(uint64(l))
	}
	if m.Data != nil {
		l = m.Data.Size()
		n += 1 + l + sovEvent(uint64(l))
	}
	l = len(m.EventName)
	if l > 0 {
		n += 1 + l + sovEvent(uint64(l))
	}
	l = len(m.ProcessId)
	if l > 0 {
		n += 1 + l + sovEvent(uint64(l))
	}
	if m.Timestamp != nil {
		l = github_com_gogo_protobuf_types.SizeOfStdTime(*m.Timestamp)
		n += 1 + l + sovEvent(uint64(l))
	}
	return n
}

func sovEvent(x uint64) (n int) {
	for {
		n++
		x >>= 7
		if x == 0 {
			break
		}
	}
	return n
}
func sozEvent(x uint64) (n int) {
	return sovEvent(uint64((x << 1) ^ uint64((int64(x) >> 63))))
}
func (m *Event) Unmarshal(dAtA []byte) error {
	l := len(dAtA)
	iNdEx := 0
	for iNdEx < l {
		preIndex := iNdEx
		var wire uint64
		for shift := uint(0); ; shift += 7 {
			if shift >= 64 {
				return ErrIntOverflowEvent
			}
			if iNdEx >= l {
				return io.ErrUnexpectedEOF
			}
			b := dAtA[iNdEx]
			iNdEx++
			wire |= (uint64(b) & 0x7F) << shift
			if b < 0x80 {
				break
			}
		}
		fieldNum := int32(wire >> 3)
		wireType := int(wire & 0x7)
		if wireType == 4 {
			return fmt.Errorf("proto: Event: wiretype end group for non-group")
		}
		if fieldNum <= 0 {
			return fmt.Errorf("proto: Event: illegal tag %d (wire type %d)", fieldNum, wire)
		}
		switch fieldNum {
		case 1:
			if wireType != 2 {
				return fmt.Errorf("proto: wrong wireType = %d for field Id", wireType)
			}
			var stringLen uint64
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return ErrIntOverflowEvent
				}
				if iNdEx >= l {
					return io.ErrUnexpectedEOF
				}
				b := dAtA[iNdEx]
				iNdEx++
				stringLen |= (uint64(b) & 0x7F) << shift
				if b < 0x80 {
					break
				}
			}
			intStringLen := int(stringLen)
			if intStringLen < 0 {
				return ErrInvalidLengthEvent
			}
			postIndex := iNdEx + intStringLen
			if postIndex > l {
				return io.ErrUnexpectedEOF
			}
			m.Id = string(dAtA[iNdEx:postIndex])
			iNdEx = postIndex
		case 2:
			if wireType != 0 {
				return fmt.Errorf("proto: wrong wireType = %d for field Version", wireType)
			}
			m.Version = 0
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return ErrIntOverflowEvent
				}
				if iNdEx >= l {
					return io.ErrUnexpectedEOF
				}
				b := dAtA[iNdEx]
				iNdEx++
				m.Version |= (int32(b) & 0x7F) << shift
				if b < 0x80 {
					break
				}
			}
		case 3:
			if wireType != 2 {
				return fmt.Errorf("proto: wrong wireType = %d for field Actor", wireType)
			}
			var stringLen uint64
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return ErrIntOverflowEvent
				}
				if iNdEx >= l {
					return io.ErrUnexpectedEOF
				}
				b := dAtA[iNdEx]
				iNdEx++
				stringLen |= (uint64(b) & 0x7F) << shift
				if b < 0x80 {
					break
				}
			}
			intStringLen := int(stringLen)
			if intStringLen < 0 {
				return ErrInvalidLengthEvent
			}
			postIndex := iNdEx + intStringLen
			if postIndex > l {
				return io.ErrUnexpectedEOF
			}
			m.Actor = string(dAtA[iNdEx:postIndex])
			iNdEx = postIndex
		case 4:
			if wireType != 2 {
				return fmt.Errorf("proto: wrong wireType = %d for field Data", wireType)
			}
			var msglen int
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return ErrIntOverflowEvent
				}
				if iNdEx >= l {
					return io.ErrUnexpectedEOF
				}
				b := dAtA[iNdEx]
				iNdEx++
				msglen |= (int(b) & 0x7F) << shift
				if b < 0x80 {
					break
				}
			}
			if msglen < 0 {
				return ErrInvalidLengthEvent
			}
			postIndex := iNdEx + msglen
			if postIndex > l {
				return io.ErrUnexpectedEOF
			}
			if m.Data == nil {
				m.Data = &google_protobuf.Any{}
			}
			if err := m.Data.Unmarshal(dAtA[iNdEx:postIndex]); err != nil {
				return err
			}
			iNdEx = postIndex
		case 5:
			if wireType != 2 {
				return fmt.Errorf("proto: wrong wireType = %d for field EventName", wireType)
			}
			var stringLen uint64
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return ErrIntOverflowEvent
				}
				if iNdEx >= l {
					return io.ErrUnexpectedEOF
				}
				b := dAtA[iNdEx]
				iNdEx++
				stringLen |= (uint64(b) & 0x7F) << shift
				if b < 0x80 {
					break
				}
			}
			intStringLen := int(stringLen)
			if intStringLen < 0 {
				return ErrInvalidLengthEvent
			}
			postIndex := iNdEx + intStringLen
			if postIndex > l {
				return io.ErrUnexpectedEOF
			}
			m.EventName = string(dAtA[iNdEx:postIndex])
			iNdEx = postIndex
		case 6:
			if wireType != 2 {
				return fmt.Errorf("proto: wrong wireType = %d for field ProcessId", wireType)
			}
			var stringLen uint64
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return ErrIntOverflowEvent
				}
				if iNdEx >= l {
					return io.ErrUnexpectedEOF
				}
				b := dAtA[iNdEx]
				iNdEx++
				stringLen |= (uint64(b) & 0x7F) << shift
				if b < 0x80 {
					break
				}
			}
			intStringLen := int(stringLen)
			if intStringLen < 0 {
				return ErrInvalidLengthEvent
			}
			postIndex := iNdEx + intStringLen
			if postIndex > l {
				return io.ErrUnexpectedEOF
			}
			m.ProcessId = string(dAtA[iNdEx:postIndex])
			iNdEx = postIndex
		case 7:
			if wireType != 2 {
				return fmt.Errorf("proto: wrong wireType = %d for field Timestamp", wireType)
			}
			var msglen int
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return ErrIntOverflowEvent
				}
				if iNdEx >= l {
					return io.ErrUnexpectedEOF
				}
				b := dAtA[iNdEx]
				iNdEx++
				msglen |= (int(b) & 0x7F) << shift
				if b < 0x80 {
					break
				}
			}
			if msglen < 0 {
				return ErrInvalidLengthEvent
			}
			postIndex := iNdEx + msglen
			if postIndex > l {
				return io.ErrUnexpectedEOF
			}
			if m.Timestamp == nil {
				m.Timestamp = new(time.Time)
			}
			if err := github_com_gogo_protobuf_types.StdTimeUnmarshal(m.Timestamp, dAtA[iNdEx:postIndex]); err != nil {
				return err
			}
			iNdEx = postIndex
		default:
			iNdEx = preIndex
			skippy, err := skipEvent(dAtA[iNdEx:])
			if err != nil {
				return err
			}
			if skippy < 0 {
				return ErrInvalidLengthEvent
			}
			if (iNdEx + skippy) > l {
				return io.ErrUnexpectedEOF
			}
			iNdEx += skippy
		}
	}

	if iNdEx > l {
		return io.ErrUnexpectedEOF
	}
	return nil
}
func skipEvent(dAtA []byte) (n int, err error) {
	l := len(dAtA)
	iNdEx := 0
	for iNdEx < l {
		var wire uint64
		for shift := uint(0); ; shift += 7 {
			if shift >= 64 {
				return 0, ErrIntOverflowEvent
			}
			if iNdEx >= l {
				return 0, io.ErrUnexpectedEOF
			}
			b := dAtA[iNdEx]
			iNdEx++
			wire |= (uint64(b) & 0x7F) << shift
			if b < 0x80 {
				break
			}
		}
		wireType := int(wire & 0x7)
		switch wireType {
		case 0:
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return 0, ErrIntOverflowEvent
				}
				if iNdEx >= l {
					return 0, io.ErrUnexpectedEOF
				}
				iNdEx++
				if dAtA[iNdEx-1] < 0x80 {
					break
				}
			}
			return iNdEx, nil
		case 1:
			iNdEx += 8
			return iNdEx, nil
		case 2:
			var length int
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return 0, ErrIntOverflowEvent
				}
				if iNdEx >= l {
					return 0, io.ErrUnexpectedEOF
				}
				b := dAtA[iNdEx]
				iNdEx++
				length |= (int(b) & 0x7F) << shift
				if b < 0x80 {
					break
				}
			}
			iNdEx += length
			if length < 0 {
				return 0, ErrInvalidLengthEvent
			}
			return iNdEx, nil
		case 3:
			for {
				var innerWire uint64
				var start int = iNdEx
				for shift := uint(0); ; shift += 7 {
					if shift >= 64 {
						return 0, ErrIntOverflowEvent
					}
					if iNdEx >= l {
						return 0, io.ErrUnexpectedEOF
					}
					b := dAtA[iNdEx]
					iNdEx++
					innerWire |= (uint64(b) & 0x7F) << shift
					if b < 0x80 {
						break
					}
				}
				innerWireType := int(innerWire & 0x7)
				if innerWireType == 4 {
					break
				}
				next, err := skipEvent(dAtA[start:])
				if err != nil {
					return 0, err
				}
				iNdEx = start + next
			}
			return iNdEx, nil
		case 4:
			return iNdEx, nil
		case 5:
			iNdEx += 4
			return iNdEx, nil
		default:
			return 0, fmt.Errorf("proto: illegal wireType %d", wireType)
		}
	}
	panic("unreachable")
}

var (
	ErrInvalidLengthEvent = fmt.Errorf("proto: negative length found during unmarshaling")
	ErrIntOverflowEvent   = fmt.Errorf("proto: integer overflow")
)

func init() { proto.RegisterFile("proto/event.proto", fileDescriptorEvent) }

var fileDescriptorEvent = []byte{
	// 272 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0x64, 0x8f, 0x41, 0x4e, 0x84, 0x30,
	0x14, 0x86, 0xf3, 0x10, 0x66, 0x42, 0x35, 0x26, 0x36, 0xb3, 0xa8, 0xc4, 0x30, 0xc4, 0x15, 0x1b,
	0x21, 0xd1, 0xbd, 0x89, 0x93, 0xb8, 0x70, 0xe3, 0x82, 0x78, 0x81, 0x02, 0xb5, 0x36, 0x0e, 0x3c,
	0x02, 0x1d, 0x92, 0xb9, 0x85, 0xc7, 0x72, 0xe9, 0x0d, 0x34, 0xec, 0xbd, 0x83, 0xa1, 0x95, 0x21,
	0xd1, 0x5d, 0xff, 0xff, 0xff, 0x5e, 0xff, 0xf7, 0xc8, 0x59, 0xd3, 0xa2, 0xc6, 0x54, 0xf4, 0xa2,
	0xd6, 0x89, 0x79, 0x53, 0xb7, 0xdf, 0x8a, 0xd7, 0xe0, 0x5c, 0x22, 0xca, 0xad, 0x48, 0x8d, 0x97,
	0xef, 0x9e, 0x53, 0x5e, 0xef, 0x2d, 0x10, 0xac, 0xff, 0x46, 0x5a, 0x55, 0xa2, 0xd3, 0xbc, 0x6a,
	0x7e, 0x81, 0x2b, 0xa9, 0xf4, 0xcb, 0x2e, 0x4f, 0x0a, 0xac, 0x52, 0x89, 0x12, 0x67, 0x72, 0x54,
	0xb6, 0x71, 0x7c, 0x59, 0xfc, 0xf2, 0x1b, 0x88, 0x77, 0x3f, 0x2e, 0x40, 0x4f, 0x89, 0xa3, 0x4a,
	0x06, 0x11, 0xc4, 0x7e, 0xe6, 0xa8, 0x92, 0x32, 0xb2, 0xec, 0x45, 0xdb, 0x29, 0xac, 0x99, 0x13,
	0x41, 0xec, 0x65, 0x93, 0xa4, 0x2b, 0xe2, 0xf1, 0x42, 0x63, 0xcb, 0x8e, 0x0c, 0x6c, 0x05, 0x8d,
	0x89, 0x5b, 0x72, 0xcd, 0x99, 0x1b, 0x41, 0x7c, 0x7c, 0xbd, 0x4a, 0xec, 0xa2, 0xc9, 0x54, 0x9f,
	0xdc, 0xd5, 0xfb, 0xcc, 0x10, 0xf4, 0x82, 0xf8, 0xe6, 0xe6, 0x47, 0x5e, 0x09, 0xe6, 0x99, 0x3f,
	0x66, 0x63, 0x4c, 0x9b, 0x16, 0x0b, 0xd1, 0x75, 0x0f, 0x25, 0x5b, 0xd8, 0xf4, 0x60, 0xd0, 0x5b,
	0xe2, 0x1f, 0x2e, 0x66, 0x4b, 0x53, 0x15, 0xfc, 0xab, 0x7a, 0x9a, 0x88, 0x8d, 0xfb, 0xf6, 0xb9,
	0x86, 0x6c, 0x1e, 0xd9, 0x9c, 0xbc, 0x0f, 0x21, 0x7c, 0x0c, 0x21, 0x7c, 0x0d, 0x21, 0xe4, 0x0b,
	0x33, 0x72, 0xf3, 0x13, 0x00, 0x00, 0xff, 0xff, 0x16, 0x4b, 0x76, 0xe6, 0x8a, 0x01, 0x00, 0x00,
}
