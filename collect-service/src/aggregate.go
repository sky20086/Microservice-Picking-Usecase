package main

import (
	"time"
	"errors"
	"log"
	. "./proto"
	. "./proto/warehouse"
	"github.com/gogo/protobuf/types"
	"github.com/gogo/protobuf/proto"
)

const (
	PICKING_ID_MISSING       = "Picking Id is required"
	ALREADY_DELETED_ERROR    = "Collect is already deleted"
	NOT_CREATED_ERROR        = "Collect is not yet created"
	ARTICLE_NOT_FOUND_ERROR  = "Article %v not found"
)

// Creates new Collect Aggregate with generated Id and Version -1
func NewCollectAggregate(id string, history []*Event) Aggregate {
	a := &CollectAggregate{
		id:      id,
		version: 0,
	}

	for _, event := range history {
		if event != nil {
			a.Apply(event)
		}
	}

	return a
}

// ******************
// *   HANDLE CMDS  *
// ******************

// Validates Command against the Aggregate and Issues 1 to n Events or fails with error
func (a *CollectAggregate) Handle(cmd *Command) (events []*Event, err error) {
	// Generic Validation
	if a.Deleted {
		return nil, errors.New(ALREADY_DELETED_ERROR)
	}

	if a.Version() == 0 && cmd.CommandName != CREATE_COLLECT {
		return nil, errors.New(NOT_CREATED_ERROR)
	}

	// Handle Command
	switch cmd.CommandName {
	case CREATE_COLLECT:
		var data CreateCollectPayload
		if err := types.UnmarshalAny(cmd.Data, &data); err != nil {
			return nil, err
		}

		if data.Picking == "" {
			return nil, errors.New(PICKING_ID_MISSING)
		}

		events = NewEvent(COLLECT_CREATED, &CollectCreatedPayload{
			Picking:  data.Picking,
			Articles: data.Articles,
		})
	case DELETE_COLLECT:
		events = NewEvent(COLLECT_DELETED, nil)
	case COLLECT_ARTICLE:
		var data CollectArticlePayload
		if err := types.UnmarshalAny(cmd.Data, &data); err != nil {
			return nil, err
		}
		var found bool
		for article, _ := range a.Articles  {
			if article == data.Article {
				events = NewEvent(ARTICLE_COLLECTED, &ArticleCollectedPayload{Article:data.Article})
				found = true
			}
		}
		if !found {
			return nil, errors.New(ARTICLE_NOT_FOUND_ERROR)
		}
		completed := true
		for article, collectedArticle := range a.CollectedArticles{
			completed = completed && (article == data.Article || collectedArticle)
		}
		if completed {
			events = append(events, NewEvent(COLLECT_COMPLETED, nil)...)
		}
	case ASSIGN_COLLECTOR:
		var data AssignCollectorPayload
		if err := types.UnmarshalAny(cmd.Data, &data); err != nil {
			return nil, err
		}
		if data.User == "" {
			return nil, errors.New("Assigned User can't be empty")
		}
		events = NewEvent(COLLECTOR_ASSIGNED, &CollectorAssignedPayload{User:data.User})
	default:
		return nil, errors.New("Unknown command. " + cmd.CommandName)
	}

	if err != nil {
		return nil, err
	}

	// Add event meta info
	for i, evn := range events {
		evn.Id = a.Id()
		evn.Version = int32(a.Version() + i + 1)
		evn.Actor = cmd.Actor
		evn.ProcessId = cmd.ProcessId

		now := time.Now()
		evn.Timestamp = &now
	}

	return events, nil
}

// ******************
// *  APPLY EVENTS  *
// ******************

// Apply Events to Event Aggregate
func (a *CollectAggregate) Apply(event *Event) error {
	if a.Id() != event.Id {
		log.Fatalf("Try to apply event to aggregate with missmatching IDs.\n\tAggregate: %v\n\tEvent: %v", a.Id, event)
	}

	switch event.EventName {
	case COLLECT_CREATED:
		var data CollectCreatedPayload
		if err := types.UnmarshalAny(event.Data, &data); err != nil {
			return err
		}

		if data.Articles != nil {
			a.Articles = data.Articles
		} else {
			a.Articles = map[string]int32{}
		}

		a.CollectedArticles = make(map[string]bool)
		for article := range a.Articles {
			a.CollectedArticles[article] = false
		}
	case COLLECT_DELETED:
		a.Deleted = true
	case COLLECTOR_ASSIGNED:
		var data CollectorAssignedPayload
		if err := types.UnmarshalAny(event.Data, &data); err != nil {
			return err
		}
		a.User = data.User
	case ARTICLE_COLLECTED:
		var data ArticleCollectedPayload
		if err := types.UnmarshalAny(event.Data, &data); err != nil {
			return err
		}
		a.CollectedArticles[data.Article] = true
	default:
		log.Fatalf("Unknown Event: %v", event.EventName)
	}

	a.version = int(event.Version)

	return nil
}

// generic
func NewEvent(name string, payload proto.Message) []*Event {
	var data *types.Any

	if payload != nil {
		var err error
		data, err = types.MarshalAny(payload)

		if err != nil {
			panic(err)
		}
	} else {
		data = nil
	}

	return []*Event{
		{
			EventName: name,
			Data:      data,
		}}
}
