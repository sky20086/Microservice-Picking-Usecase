﻿using System;
using System.Reactive;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using Vlek;
using static System.Environment;

namespace PickingProcessor
{
    class Program
    {
        static void Main()
        {
            var natsUrl = GetEnvironmentVariable("NATS_URL") ?? "nats://localhost:4222";
            var cluster = GetEnvironmentVariable("STAN_CLUSTER") ?? "test-cluster";
            var client = GetEnvironmentVariable("STAN_CLIENT") ?? "picking-processor";
            var eventStoreUrl = GetEnvironmentVariable("EVENT_STORE") ?? "localhost:8081";
            
            var stream = new EventStream(natsUrl, cluster, client, eventStoreUrl);

            stream
                .Events("PickingCreated")
                .Buffer(TimeSpan.FromHours(1))
                .Select(val => new Event
                {
                    EventName = "PickingCompleted"
                })
                .Subscribe(stream.Store);
            
            // Cancel thread with Ctrl + C
            var subject = new Subject<Unit>();
            Console.CancelKeyPress += (_, args) =>
            {
                if (args.Cancel)
                {
                    subject.OnCompleted();
                }
            };
            subject.Wait();
            
            stream.Dispose();
        }
    }
}
