module.exports = function (pbjs) {
  pbjs.wrappers['.google.protobuf.Any'] = {
    fromObject: function (object) {
      // unwrap value type if mapped
      if (object && object['@type']) {
        var type = this.lookup(object['@type'].match(/(?:.*\/)?(.*)?$/)[1])
        /* istanbul ignore else */
        if (type) {
          // type_url does not accept leading "."
          var type_url = object['@type'].charAt(0) === '.' ? object['@type'].substr(1) : object['@type']
          type_url = type_url.match(/.*\/.*/) ? type_url : 'type.googleapis.com/' + type_url
          return this.create({
            type_url: type_url,
            value: type.encode(type.fromObject(object)).finish()
          })
        }
      }

      return this.fromObject(object)
    },

    toObject: function (message, options) {
      console.log(message)


      // decode value if requested and unmapped
      if (message.type_url && message.value) {
        var type = this.lookup(message.type_url.match(/(?:.*\/)?(.*)?$/)[1])
        /* istanbul ignore else */
        if (type)
          message = type.decode(message.value)
      }

      // wrap value if unmapped
      if (!(message instanceof this.ctor) && message instanceof pbjs.Message) {
        var object = message.$type.toObject(message, options)
        object['@type'] = message.$type.fullName
        return object
      }

      return this.toObject(message, options)
    }
  }

  pbjs.wrappers['.google.protobuf.Timestamp'] = {
    fromObject: function (object) {
      // unwrap value type if mapped
      if (object instanceof Date) {
        var time = object.getTime()

        return this.create({
          seconds: Math.trunc(time / 1000),
          nanos: (time % 1000 * 1000000)
        })
      }

      return this.fromObject(object)
    },

    toObject: function (message, options) {
      // decode value if requested and unmapped
      if (message.seconds) {
        var t = new Date(Date.UTC(1970, 0, 1))
        t.setUTCSeconds(message.seconds)
        if (message.nanos) {
          t.setUTCMilliseconds(message.nanos / 1000000)
        }
        return t
      }

      return this.toObject(message, options)
    }
  }
}
