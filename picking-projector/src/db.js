import mongoose from 'mongoose'

mongoose.connect(
  process.env.MONGO_URL || 'mongodb://localhost/test',
  {useMongoClient: true, promiseLibrary: global.Promise}
)

export const Picking = mongoose.model('Picking', new mongoose.Schema({}, {strict: false}))

export const Collection = mongoose.model('Collection', new mongoose.Schema({}, {strict: false}))

export const Control = mongoose.model('Control', new mongoose.Schema({}, {strict: false}))