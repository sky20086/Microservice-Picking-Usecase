# Entities

## Collect Aggregate

```mermaid
sequenceDiagram;
  participant Actor;
  participant Collect;
  participant Event Stream;

  Actor ->> Collect: CREATE_COLLECT;
  Collect -->> Event Stream: COLLECT_CREATED;

  Actor ->> Collect: ASSIGN_COLLECTOR;
  Collect -->> Event Stream: COLLECTOR_ASSIGNED;
  Actor ->> Collect: COLLECT_ARTICLE;
  Collect -->> Event Stream: ARTICLE_COLLECTED;
  opt all articles collected;
    Collect -->> Event Stream: COLLECT_COMPLETED;
  end;

  Actor ->> Collect: DELETE_COLLECT;
  Collect -->> Event Stream: COLLECT_DELETED;
```

## Control Aggregate

```mermaid
sequenceDiagram;
  participant Actor;
  participant Control;
  participant Event Stream;

  Actor ->> Control: CREATE_CONTROL;
  Control -->> Event Stream: CONTROL_CREATED;

  Actor ->> Control: ASSIGN_CONTROLLER;
  Control -->> Event Stream: CONTROLLER_ASSIGNED;
  Actor ->> Control: CONTROL_ARTICLE;
  Control -->> Event Stream: ARTICLE_CONTROLED;
  opt all articles confirmed;
    Control -->> Event Stream: CONTROL_COMPLETED;
  end;

  Actor ->> Control: DELETE_CONTROL;
  Control -->> Event Stream: CONTROL_DELETED;
```


## Picking Process Manager

```mermaid
sequenceDiagram;
  participant Legacy;
  participant Process Manager;
  participant Collect;
  participant Control;

  Legacy --x Process Manager: PICKING_CREATED;

  Process Manager ->> Collect: CREATE_COLLECT;
  Collect --x Process Manager: COLLECT_COMPLETED;

  Process Manager ->> Control: CREATE_CONTROL;
  Control --x Process Manager: CONTROL_COMPLETED;

  Process Manager ->> Legacy: COMPLETE_PICKING;
```