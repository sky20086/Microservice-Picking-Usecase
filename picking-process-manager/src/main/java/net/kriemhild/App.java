package net.kriemhild;

import com.google.protobuf.InvalidProtocolBufferException;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.nats.stan.Connection;
import io.nats.stan.ConnectionFactory;
import io.nats.stan.MessageHandler;
import io.nats.stan.SubscriptionOptions;
import warehouse.CommandHandlerGrpc;
import vlek.Event;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.concurrent.TimeoutException;

public class App {
    private static Connection connection;

    static String getEnv(String key, String def) {
        String env = System.getenv(key);
        if (env != null)
            return env;
        return def;
    }

    static int getPort(String key, int def) {
        String env = System.getenv(key);
        if (env != null)
            return Integer.valueOf(env);
        return def;
    }


    public static void main(String[] args) throws IOException, TimeoutException {
        System.out.println("Starting ProcessManager");

        PickingProcessStore store = new InMemPickingProcessStore();
        CommandHandlerWrapper collect = new CommandHandlerWrapper(commandHandlerBlockingStub(getEnv("COLLECT_HOST", "localhost"), getPort("COLLECT_PORT", 80)));
        CommandHandlerWrapper control = new CommandHandlerWrapper(commandHandlerBlockingStub(getEnv("CONTROL_HOST", "localhost"), getPort("CONTROL_PORT", 80)));
        CommandHandlerWrapper picking = new CommandHandlerWrapper(commandHandlerBlockingStub(getEnv("PICKING_HOST", "localhost"), getPort("PICKING_PORT", 80)));

        ConnectionFactory cf = new ConnectionFactory(System.getenv("STAN_CLUSTER"), System.getenv("STAN_CLIENT"));
        PickingProcessManager pickingProcessManager = new PickingProcessManager(store, collect, control, picking);

        cf.setNatsUrl(getEnv("NATS_URL", "nats://localhost:4222"));
        connection = cf.createConnection();

        // GRPC Messagehandler
        MessageHandler messageHandler = message -> {
            try {
                Event event = Event.parseFrom(message.getData());
                pickingProcessManager.handle(event);
            } catch (InvalidProtocolBufferException e) {
                System.out.println(String.format("Can't parse event from message:%s", message.toString()));
            } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
                System.out.println(String.format("Can't access method to parse event from message:%s", message.toString()));
            }
        };

        // Due to consistent naming, the eventNames can be extracted from handle methods.
        for (Method m : PickingProcessManager.class.getMethods()) {
            if (m.getName().startsWith("handle")) {
                String eventName = m.getName().substring(6);
                if (eventName.isEmpty())
                    continue;
                subscribeToEvent(messageHandler, eventName);
                System.out.println(String.format("Subscribed to Event %s", eventName));
            }
        }
    }

    /**
     * Subscribe Nats Messagehandler to events. Errors can be catched as NATS provides autoreconnect.
     *
     * @param messageHandler NATS Messagehandler
     * @param eventName      have to be consistent. Also for the handle-methods.
     */
    private static void subscribeToEvent(MessageHandler messageHandler, String eventName) {
        try {
            connection.subscribe(eventName, messageHandler, new SubscriptionOptions.Builder().setDurableName("net.kriemhild.PickingProcessManager").build());
        } catch (IOException | TimeoutException e) {
            System.out.println(String.format("Can't subscribe to messageHandler for %s", eventName));
        }
    }

    public static CommandHandlerGrpc.CommandHandlerBlockingStub commandHandlerBlockingStub(String host, int port) {
        ManagedChannel channel = ManagedChannelBuilder
                .forAddress(host, port)
                .usePlaintext(true)
                .build();
        System.out.println(String.format("Using event store: %s:%d", host, port));

        return CommandHandlerGrpc.newBlockingStub(channel);
    }
}
