package net.kriemhild;

import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;

/**
 * In Memory Picking Process Store
 */
public class InMemPickingProcessStore implements PickingProcessStore {
    private ConcurrentHashMap<String, PickingProcess> cache = new ConcurrentHashMap<>();

    public PickingProcess getById(String id) {
        return this.cache.getOrDefault(id, new PickingProcess(id));
    }

    public PickingProcess getByCollectId(String id) {
        for (PickingProcess process: this.cache.values()) {
            if(Objects.equals(process.collectId, id))
                return process;
        }

        return new PickingProcess(id);
    }

    public PickingProcess getByControlId(String id) {
        for (PickingProcess process: this.cache.values()) {
            if(Objects.equals(process.controlId, id))
                return process;
        }

        return new PickingProcess(id);
    }

    public void save(PickingProcess process){
        cache.put(process.id, process);
    }
}
