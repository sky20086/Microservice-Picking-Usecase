const server = require('graphql-server-micro')
const {makeExecutableSchema} = require('graphql-tools')
const fetch = require('node-fetch')
const {Collection, Picking, Control} = require('./db')

const legacy_system = process.env.LEGACY || 'http://localhost:8001'

const typeDefs = `
  schema {
    query: Query
    mutation: Mutation
  }
 
  type Article {
    id: String
    amount: Int  
  } 
 
  type Picking {
    id: String
    articles: [Article]
  }
  
  type Query {
    pickings: [Picking]
    collections: [Collection]
  }
  
  type Mutation {
    startPicking: Boolean
  }
  
  type Collection {
    id: String
    articles: [Article]
  }
`

const resolvers = {
  Query: {
    pickings: () => Picking.find().lean(),
    collections: () => Collection.find().lean()
  },

  Mutation: {
    startPicking: async () => {
      const res = await fetch(`${legacy_system}/createPicking`)
      return res.ok
    }
  },
  Picking: {
    id: (p) => p._id
  },
  Collection: {
    id: (c) => c._id
  }
}

module.exports = server.microGraphql({schema: makeExecutableSchema({typeDefs, resolvers})})
