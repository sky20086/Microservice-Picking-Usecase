const pbjs = require('protobufjs')
const uuid = require('uuid/v4')
require('./pbjs-wrapper')(pbjs)

const grpc = require('grpc')

const protos = pbjs.loadSync([
  __dirname + '/proto/event.proto',
  __dirname + '/proto/event-store.proto',
  __dirname + '/proto/picking-events.proto'
])

const EventStore = grpc.loadObject(protos).vlek.EventStore
const store = new EventStore(
  process.env.EVENT_STORE || 'localhost:8000',
  grpc.credentials.createInsecure())

const {send} = require('micro')
const {router, get} = require('microrouter')

const storeEvent = (e) => new Promise((resolve, reject) => store.store(e, (err) => err ? reject(err) : resolve()))
const aggregateEvents = (id) => new Promise((resolve, reject) => store.aggregate({id}, (err, resp) => err ? reject(err) : resolve(resp)))

async function sendP (req, res) {
  try {
    let id = uuid()
    await storeEvent({
      id,
      version: 0,
      actor: uuid(),
      data: {
        '@type': 'warehouse.PickingCreatedPayload',
        order: '1234',
        articles: {
          'article1': 3,
          '12234':6
        }
      },
      eventName: 'PickingCreated',
      processId: uuid(),
      timestamp: new Date()
    })

    let resp = await aggregateEvents(id)

    return send(res, 200, JSON.stringify(resp.events))
  } catch (err) {
    console.log(err)
    return send(res, 400, err)
  }
}

function getArticle(req, res) {
    return send(res, 200, 'RITALIN 10 MG Tabletten')
}

module.exports = router(
  get('/createPicking', sendP),
  get('/article', getArticle)
)
