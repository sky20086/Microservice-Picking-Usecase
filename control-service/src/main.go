package main

import (
	"net"
	"log"
	."./proto"
	. "./proto/warehouse"
	"google.golang.org/grpc"
)

func main() {
	port := GetEnv("PORT", "8082")
	eventStoreUrl := GetEnv("EVENT_STORE", "localhost:8081")

	lis, err := net.Listen("tcp", ":"+port)

	if err != nil {
		log.Fatal("Error listening tcp. Error: " + err.Error())
		return
	}

	// Store
	CheckgRPCUrl(eventStoreUrl)

	esc, err := grpc.Dial(eventStoreUrl, grpc.WithInsecure())
	if err != nil {
		log.Fatal("Error connecting event store. Error: " + err.Error())
		return
	}
	defer esc.Close()
	log.Printf("CONNECTED TO EVENT STORE AT: %v", eventStoreUrl)

	// Command Handler
	handler := &commandHandler{
		store:        NewEventStoreClient(esc),
		newAggregate: NewControlAggregate,
	}

	grpcServer := grpc.NewServer()
	RegisterCommandHandlerServer(grpcServer, handler)

	log.Printf("LISTENING ON PORT: %v", port)

	grpcServer.Serve(lis)
}
