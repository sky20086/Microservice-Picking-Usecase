package main

import (
	."./proto"
	. "./proto/warehouse"
	"golang.org/x/net/context"
	"github.com/gogo/protobuf/types"
	"log"
	"github.com/satori/go.uuid"
	"errors"
)

type Aggregate interface {
	Handle(cmd *Command) (events []*Event, err error)
	Apply(event *Event) error

	Id() string
	Version() int
}

type commandHandler struct {
	newAggregate func(string, []*Event) Aggregate

	store EventStoreClient
}

// Loads aggregate from store, tries to apply command and send spawned events back to store
func (c *commandHandler) Handle(ctx context.Context, cmd *Command) (*types.Empty, error) {
	if cmd.Id == "" {
		return nil, errors.New("Missing Aggregate Id (id)")
	}

	if cmd.Actor == "" {
		return nil, errors.New("Missing Actor Id (actor)")
	}

	if cmd.CommandName == "" {
		return nil, errors.New("Missing Command Name (commandName)")
	}

	if cmd.ProcessId == "" {
		idd, _ := uuid.NewV4()

		cmd.ProcessId =idd.String()
	}

	history, err := c.store.Aggregate(ctx, &AggregateRequest{Id: cmd.Id})
	if err != nil {
		return nil, err
	}

	a := c.newAggregate(cmd.Id, history.Events)

	log.Printf("Handle cmd for %v cmd: %+v", cmd.Id, *cmd)

	if events, err := a.Handle(cmd); err != nil {
		return nil, err
	} else {
		// TODO: Store should be extracted into aggregate go runtime with restarts until all events are stored.
		// Store events
		for _, event := range events {
			if _, err := c.store.Store(ctx, event); err != nil {
				return nil, err
			}
		}
	}

	return &types.Empty{}, nil
}
