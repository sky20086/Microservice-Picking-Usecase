package main

// ******************
// *   AGGREGATES   *
// ******************

type ControlAggregate struct {
	id      string
	version int
	Deleted bool

	Picking string
	User    string

	Articles           map[string]int32
	ControlledArticles map[string]bool
}

func (a *ControlAggregate) Id() string   { return a.id }
func (a *ControlAggregate) Version() int { return a.version }

// ******************
// *	COMMANDS	*
// ******************

const CREATE_CONTROL = "CONTROL.CREATE_CONTROL"

const DELETE_CONTROL = "CONTROL.DELETE_CONTROL"

const CONTROL_ARTICLE = "CONTROL.CONTROL_ARTICLE"

const ASSIGN_CONTROLOR = "CONTROL.ASSIGN_CONTROLLER"

// ******************
// *     EVENTS     *
// ******************

const CONTROL_CREATED = "CONTROL.CONTROL_CREATED"

const CONTROL_DELETED = "CONTROL.CONTROL_DELETED"

const ARTICLE_CONTROLLED = "CONTROL.ARTICLE_CONTROLLED"

const CONTROLLER_ASSIGNED = "CONTROL.CONTROLLER_ASSIGNED"

const CONTROL_COMPLETED = "CONTROL.CONTROL_COMPLETED"
