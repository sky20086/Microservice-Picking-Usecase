package main

import (
	"github.com/satori/go.uuid"
	. "github.com/onsi/gomega"
	"fmt"
	"time"
	"errors"
	"github.com/gogo/protobuf/types"
)

func ptr(t time.Time) *time.Time {
	return &t
}

type UuidMatcher struct{}

func BeUUID() OmegaMatcher {
	return &UuidMatcher{}
}

func (matcher *UuidMatcher) Match(actual interface{}) (success bool, err error) {
	if val, ok := actual.(string); ok {
		if _, err := uuid.FromString(val); err != nil {
			return false, err
		} else {
			return true, nil
		}
	}

	return false, errors.New("No String")
}

func (matcher *UuidMatcher) FailureMessage(actual interface{}) (message string) {
	return fmt.Sprintf("Expected %#v to to be aggregate UUID", actual)
}

func (matcher *UuidMatcher) NegatedFailureMessage(actual interface{}) (message string) {
	return fmt.Sprintf("Expected %#v not to to be aggregate UUID", actual)
}

type UnpackMatcher struct {
	Matcher OmegaMatcher

	// Failure message.
	failure string
}

func UnpackTo(matcher OmegaMatcher) OmegaMatcher {
	return &UnpackMatcher{
		Matcher: matcher,
	}
}
func (m *UnpackMatcher) Match(actual interface{}) (success bool, err error) {
	any, ok := actual.(*types.Any)

	if !ok {
		return false, fmt.Errorf("UnpackMatcher expects protobuf type *Any but we have %#v", actual)
	}

	var msg types.DynamicAny

	if err := types.UnmarshalAny(any, &msg); err != nil {
		return false, err
	}

	match, err := m.Matcher.Match(msg.Message)
	if !match {
		m.failure = m.Matcher.FailureMessage(msg)
	}
	return match, err
}

func (m *UnpackMatcher) FailureMessage(actual interface{}) (message string) {
	return m.failure
}

func (m *UnpackMatcher) NegatedFailureMessage(actual interface{}) (message string) {
	return m.failure
}

type TimeMatcher struct {
	Expected time.Time
}

func MatchesTime(expected time.Time) OmegaMatcher {
	return &TimeMatcher{
		Expected: expected,
	}
}

func (m *TimeMatcher) Match(actual interface{}) (success bool, err error) {
	if t, ok := actual.(time.Time); !ok {
		return false, fmt.Errorf("TimeMatcher expected time but got %#v", actual)
	} else {
		if m.Expected.Equal(t) {
			return true, nil
		} else {
			return false, fmt.Errorf("Expected\n\t%#v\nto match:\n\t%#v", actual, m.Expected)
		}
	}

	return false, errors.New("No String")
}

func (m *TimeMatcher) FailureMessage(actual interface{}) (message string) {
	return fmt.Sprintf("Expected\n\t%#v\nto match:\n\t%#v", actual, m.Expected)
}

func (m *TimeMatcher) NegatedFailureMessage(actual interface{}) (message string) {
	return fmt.Sprintf("Expected\n\t%#v\nnot to match:\n\t%#v", actual, m.Expected)
}
