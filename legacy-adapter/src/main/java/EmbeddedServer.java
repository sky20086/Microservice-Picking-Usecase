import com.google.protobuf.Any;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.vertx.core.Vertx;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import warehouse.Event;
import warehouse.EventStoreGrpc;
import warehouse.PickingCreatedPayload;

import java.util.*;

public class EmbeddedServer {

    private static final Logger LOGGER = LoggerFactory.getLogger("LegacyAdapter");

    public static void main(String[] args) {
        EventStoreWrapper eventStore = new EventStoreWrapper(eventHandler(System.getenv("EVENT_STORE_HOST"), Integer.valueOf(System.getenv("EVENT_STORE_PORT"))));

        Vertx.vertx().createHttpServer().requestHandler(req -> {
            createPicking(eventStore);
            req.response().end("Hello World!");
        }).listen(8080);
    }

    private static void createPicking(EventStoreWrapper eventStore) {
        String orderId = "1234";
        Map<String, Integer> articles = new HashMap<>();
        articles.put("566", 4);
        articles.put("Elfe", 6);
        Event pickingCreated = Event.newBuilder()
                .setId(orderId)
                .setActor("Tom")
                .setEventName("PickingCreated")
                .setData(Any.pack(PickingCreatedPayload.newBuilder()
                        .putAllArticles(articles)
                        .build()))
                .setProcessId(UUID.randomUUID().toString())
                .build();

        eventStore.store(pickingCreated);
    }


    public static EventStoreGrpc.EventStoreStub eventHandler(String host, int port) {
        ManagedChannel channel = ManagedChannelBuilder
                .forAddress(host, port)
                .usePlaintext(true)
                .build();
        LOGGER.info("Using event store: " + host + ":" + port);

        return EventStoreGrpc.newStub(channel);
    }
}
