export default {
  api: (process.env && process.env.API_ENDPOINT) || '/api/graphql'
}